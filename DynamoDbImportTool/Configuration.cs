namespace DynamoDbImportTool
{
    public class Configuration
    {
        public AwsDynamoCredentials OriginalDb { get; set; }

        public AwsDynamoCredentials ClonedDb { get; set; }

        public string[] TablesToClone { get; set; }
    }
}