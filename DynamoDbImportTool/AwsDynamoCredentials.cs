namespace DynamoDbImportTool
{
    public class AwsDynamoCredentials
    {
        public string AccessKey { get; set; }

        public string SecretKey { get; set; }
    }
}