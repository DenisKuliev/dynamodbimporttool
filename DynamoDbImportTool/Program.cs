﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;
using Newtonsoft.Json;

namespace DynamoDbImportTool
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var configuration =
                JsonConvert.DeserializeObject<Configuration>(await File.ReadAllTextAsync("appConfig.json"));

            await Task.WhenAll(configuration.TablesToClone.Select(tableName => CloneTable(configuration, tableName)));

            Console.WriteLine("Done");
        }

        private static async Task CloneTable(Configuration configuration, string tableName)
        {
            var originalTable =
                GetDynamoTable(configuration.OriginalDb.AccessKey, configuration.OriginalDb.SecretKey, tableName, RegionEndpoint.USEast1);

            var clonedTable =
                GetDynamoTable(configuration.ClonedDb.AccessKey, configuration.ClonedDb.SecretKey, tableName, RegionEndpoint.USEast1);

            var search = originalTable.Scan(new ScanOperationConfig { });

            List<Document> data = await search.GetRemainingAsync();

            var joinedElements = string.Join('\n', data.Select(document => document.ToJson()));

            File.WriteAllText(tableName, joinedElements);

            var documentBatchWrite = clonedTable.CreateBatchWrite();

            data.ForEach(element => documentBatchWrite.AddDocumentToPut(element));

            await documentBatchWrite.ExecuteAsync();

            Console.WriteLine($"Table {tableName} cloned successfully!");
        }

        private static Table GetDynamoTable(string accessKey, string secretKey, string tableName,
            RegionEndpoint regionEndpoint)
        {
            var amazonDynamoDbClient = new AmazonDynamoDBClient(new BasicAWSCredentials(accessKey,
                secretKey), regionEndpoint);

            return Table.LoadTable(amazonDynamoDbClient, tableName);
        }
    }
}